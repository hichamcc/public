from newspaper import Article


url = 'https://www.euronews.com/2020/02/23/us-elections-bernie-sanders-widens-lead-in-third-democratic-race'

article = Article(url)

article.download()

article.parse()

print(article.authors)
