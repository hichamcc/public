from textblob import TextBlob
from textblob.sentiments import NaiveBayesAnalyzer
import nltk

nltk.download('movie_reviews')

text = '''i hate that place'''

blob = TextBlob(text)

# the default implementation
print(blob.sentences[0])
print(blob.sentences[0].sentiment)

# NaiveBayesAnalyzer
blob2 = TextBlob(text, analyzer=NaiveBayesAnalyzer())

print(blob2.sentiment)
if blob2.sentiment.classification == 'pos':
    print(blob2.sentiment.p_pos)
else:
    print(blob2.sentiment.p_neg)

# for sentence in blob.sentences:
#     print(sentence.sentiment.polarity)
