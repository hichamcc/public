from goose3 import Goose

url = 'https://www.bbc.com/news/world-51611422'

g = Goose()

article = g.extract(url=url)

print(article.title)
print(article.cleaned_text)
