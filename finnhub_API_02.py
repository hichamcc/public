import requests

''' Company Profile '''
# r = requests.get('https://finnhub.io/api/v1/stock/profile?symbol=MSFT&token=bp36dc7rh5r9d7scldo0')

''' CEO Compensation : doesnt work '''
# r = requests.get('https://finnhub.io/api/v1/stock/ceo-compensation?symbol=MSFT&token=bp36dc7rh5r9d7scldo0')

''' Company Executive '''
# r = requests.get('https://finnhub.io/api/v1/stock/executive?symbol=AAPL&token=bp36dc7rh5r9d7scldo0')

''' Recommendation Trends '''
# r = requests.get('https://finnhub.io/api/v1/stock/recommendation?symbol=MSFT&token=bp36dc7rh5r9d7scldo0')

''' Price Target '''
# r = requests.get('https://finnhub.io/api/v1/stock/price-target?symbol=NFLX&token=bp36dc7rh5r9d7scldo0')

''' Stock Upgrade/Downgrade '''
# r = requests.get('https://finnhub.io/api/v1/stock/upgrade-downgrade?symbol=AAPL&token=bp36dc7rh5r9d7scldo0')

''' Option Chain '''
# r = requests.get('https://finnhub.io/api/v1/stock/option-chain?symbol=AAPL&token=bp36dc7rh5r9d7scldo0')

''' Peers '''
# r = requests.get('https://finnhub.io/api/v1/stock/peers?symbol=NFLX&token=bp36dc7rh5r9d7scldo0')

''' Earnings Surprises '''
# r = requests.get('https://finnhub.io/api/v1/stock/earnings?symbol=AAPL&token=bp36dc7rh5r9d7scldo0')

''' Quote '''
# r = requests.get('https://finnhub.io/api/v1/quote?symbol=AAPL&token=bp36dc7rh5r9d7scldo0')

''' General News '''
# r = requests.get('https://finnhub.io/api/v1/news?category=general&token=bp36dc7rh5r9d7scldo0')

''' Company News '''
r = requests.get('https://finnhub.io/api/v1/news/AAPL?token=bp36dc7rh5r9d7scldo0')


print(r.json())
